# Laravel Job processing



## Getting started

As its laravel job processing and event listener coding .

Please follow those steps which need to be follow

1. git clone it

2. composer install

3. create database and add database and password as same need to add LOG_LEVEL=info and QUEUE_CONNECTION=database as we are creting a jobs as well so are using jobs through database

4. then run php artisan migrate 

5. Open a terminal go to directory path and run  php artisan serve it will run the server

6. and open next terminal and run php artisan queue:work command it will run worker node where job will be processing

7. than open the Postman or any other tool to hits the api end points which is (http://127.0.0.1:8000/api/submission) and need to pass json body 

 {
  "name":"Sharad",
  "email":"bohra.sharad@gmail.com",
  "message":"1212wedsd"
 }

it will return message "Data validated and processed successfully" and status code 200

and it will create log in the storage/logs laravel.log as we also run a event which listen by laravel echo system and creating log.


# Run Unit Test

1. Running Unit test you need to run php artisan test --filter SubmissionControllerTest it will run perfectly i have created 2 methods in it where test no field is blank or missing.

you can pull it via https://gitlab.com/bohra.sharad/laravel-job-processing.git

