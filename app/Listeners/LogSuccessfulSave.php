<?php

namespace App\Listeners;

use App\Events\UserSubmitted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
class LogSuccessfulSave
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use InteractsWithQueue;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UserSubmitted  $event
     * @return void
     */
   

    public function handle(UserSubmitted $event)
    {
        Log::info('User saved successfully', [
            'name' => $event->name,
            'email' => $event->email,
        ]);
    }
}
