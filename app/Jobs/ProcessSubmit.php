<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Submissions;
use App\Events\UserSubmitted;

use Illuminate\Support\Facades\Log;


class ProcessSubmit implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $name;
    protected $email;
    protected $message;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name,$email,$message)
    {
        //
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
      
Log::info('UserSubmitted event fired successfully', [
        'name' => $this->name,
        'email' => $this->email,
    ]);

         Submissions::create([
            'name' => $this->name,
            'email' => $this->email,
            'message' => $this->message,
        ]);
          
    

          event(new UserSubmitted(
                $this->name,
                $this->email
            ));

      
    }
}
