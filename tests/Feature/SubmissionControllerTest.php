<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SubmissionControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /*public function test_example()
    {
         $data = [
            'name1' => 'John Doe',
            'email' => 'johnexample.com',
            'message' => 'Hello, this is a test message',
        ];

        $response = $this->json('POST', '/api/submission', $data);
    
        $response->assertStatus(200)
            ->assertJson(['message' => 'Submission successful']);
    }*/
     use RefreshDatabase;


      public function test_it_submits_form_data()
    {
        $data = [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'message' => 'Hello, this is a test message',
        ];

        $response = $this->json('POST', '/api/submission', $data);
       
        $response->assertStatus(200)
            ->assertJson(['message' => 'Data validated and processed successfully']);
    }

    /** @test */
   public function test_it_requires_name_email_and_message()
    {
         // Test missing 'name' field
        $data = [
            'email' => 'john@example.com',
            'message' => 'This is a test message',
        ];
        $response = $this->json('POST', '/api/submission', $data);
      // dd($response->getContent());
        $response->assertStatus(422)
            ->assertJsonFragment(['error' => ["name"=>["The name field is required."]]]);

        // Test missing 'email' field
        $data = [
            'name' => 'John Doe',
            'message' => 'This is a test message',
        ];
        $response = $this->json('POST', '/api/submission', $data);
        $response->assertStatus(422)
            ->assertJsonFragment(['error' => ["email"=>["The email field is required."]]]);

        // Test missing 'message' field
        $data = [
            'name' => 'John Doe',
            'email' => 'john@example.com',
        ];
        $response = $this->json('POST', '/api/submission', $data);
        $response->assertStatus(422)
            ->assertJsonFragment(['error' => ["message"=>["The message field is required."]]]);

        // Test all fields present and valid
        $data = [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'message' => 'This is a test message',
        ];
        $response = $this->json('POST', '/api/submission', $data);
        $response->assertStatus(200)->assertJson(['message' => 'Data validated and processed successfully']);

        //dd($response->getContent());
        

    }
}
